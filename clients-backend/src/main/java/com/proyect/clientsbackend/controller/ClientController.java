package com.proyect.clientsbackend.controller;

import com.proyect.clientsbackend.exceptions.ResourcesNotFoundException;
import com.proyect.clientsbackend.model.Client;
import com.proyect.clientsbackend.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:4200")
public class ClientController {

    Logger logger = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientRepository repository;


    //este metodo sirve para listar todos los clientes
    @GetMapping("/clients")
    public List<Client> listClient() {
        logger.info("Metodo lista clientes");
        return repository.findAll();
    }

    //este metodo sirve para guardar el cliente
    @PostMapping("/clients")
    public Client saveClient(@RequestBody Client client) {
        logger.info("Metodo guardar cliente: "+ client.getName());
        String shared = this.convertSharedKey(client);
        client.setSharedKey(shared);
        return repository.save(client);
    }
    //este metodo se realiza para guardar sharedKey, tomando el nombre enviado en el request
    public String convertSharedKey(Client client){
        String sharedKey = client.getName();
        String[] split = sharedKey.split(" ");
        sharedKey = split[0].charAt(0) + split[1];
        return sharedKey;
    }

    //este metodo sirve para buscar un cliente
    @GetMapping("/clients/{id}")
    public ResponseEntity<Client> getClientId(@PathVariable Long id){
        Client client = repository.findById(id)
                .orElseThrow(() -> new ResourcesNotFoundException("No existe el cliente con el ID : " + id));
        logger.info("Metodo consultar cliente por ID: "+ id);
        return ResponseEntity.ok(client);
    }

    // este metodo sirve para consulta de cliente por sharedkey
    @GetMapping("/client")
    public List<Client> findBySharedKey(@RequestParam("sharedKey") String sharedKey) {
        logger.info("Metodo consultar cliente por ID: "+ sharedKey);
        return repository.searchBySharedKey(sharedKey);
    }

    //este metodo elimina cliente
    @DeleteMapping("/clients/{id}")
    public ResponseEntity<Map<String,Boolean>> deleteClient(@PathVariable Long id){
        Client client = repository.findById(id)
                .orElseThrow(() -> new ResourcesNotFoundException("No existe el cliente con el ID : " + id));

        repository.delete(client);
        Map<String, Boolean> respuesta = new HashMap<>();
        respuesta.put("eliminar",Boolean.TRUE);
        return ResponseEntity.ok(respuesta);
    }
}
