package com.proyect.clientsbackend.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "clients")
public class Client {
   //objeto que vamos a recibir, manejamos JPA
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "sharedKey")
    private String sharedKey;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private Long phone;

    @Column(name = "dateAt")
    private Date dateAt;

    @Column(name = "dateEnd")
    private Date dateEnd;

    public Client() {
    }

    public Client(Long id, String sharedKey, String name, String email, Long phone, Date dateAt, Date dateEnd) {
        Id = id;
        this.sharedKey = sharedKey;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.dateAt = dateAt;
        this.dateEnd = dateEnd;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getSharedKey() {
        return sharedKey;
    }

    public void setSharedKey(String sharedKey) {
        this.sharedKey = sharedKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public Date getDateAt() {
        return dateAt;
    }

    public void setDateAt(Date dateAt) {
        this.dateAt = dateAt;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

}
