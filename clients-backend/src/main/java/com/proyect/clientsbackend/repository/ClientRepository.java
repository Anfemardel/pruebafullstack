package com.proyect.clientsbackend.repository;

import com.proyect.clientsbackend.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    //implementacion de metodo de consulta por sharedKey
    @Query(value = "SELECT * FROM clients c WHERE c.shared_key LIKE concat('%', ?1, '%')",nativeQuery = true)
    List<Client> searchBySharedKey(String sharedKey);
}
