# PRUEBA FULLSTACK



## PRUEBA FULLSTACK

API REST realizada en SpringBoot utiliza DB MySQL.
Frontend Angular. 

## Clonar repositorio

git clone https://gitlab.com/Anfemardel/pruebafullstack.git

En este se encuentra backend y frontend.

### crear BD MySQL

Crear base de datos llamada test

### levantar backnend

Abrir clients-backend en IDE.

Descargar dependencias maven.

realizar RUN APP.

Se levanta en el puerto 8080.

### levantar frontend

Abrir clients-frontend en IDE.

realizar npm i para instalar dependencias.

En linea de comandos correr ng serve.

Se levanta en el puerto 4200.