import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListClientComponent } from './components/list-client/list-client.component';
import { CreateClientComponent } from './components/create-client/create-client.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path : 'clients', component:ListClientComponent},
  {path : '', redirectTo: 'clients', pathMatch:'full'},
  {path : 'create-client', component:CreateClientComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
