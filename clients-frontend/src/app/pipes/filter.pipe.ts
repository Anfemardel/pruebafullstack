import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
 //filtro para buscar por sharedKey
  transform(value: any, arg: any): any {
    if (arg == '' || arg.length<3) return value;
    const resultClient = [];
    for(const client of value){
      if(client.sharedKey != null){
        if(client.sharedKey.toLowerCase().indexOf(arg.toLowerCase()) > -1){
          resultClient.push(client);
        }
      }
    }
    return resultClient;
  }

}
