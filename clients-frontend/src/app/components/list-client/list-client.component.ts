import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/model/client';
import { ClientService } from 'src/app/services/client.service';
import { AngularCsv } from 'angular-csv-ext/dist/Angular-csv';

@Component({
  selector: 'app-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.css']
})
export class ListClientComponent implements OnInit {

  clients: Client[];

  constructor(private clientService: ClientService, private router:Router) { }

  filterClients = '';
  ngOnInit(): void {
    this.findAllClients();
  }

  //Metodo Traer lista de clientes
  private findAllClients(){
    this.clientService.getClients().subscribe(dato => {
      console.log(dato);
      this.clients = dato;
    });
  }

  //Metodo eliminar clientes
  deleteClient(id:number){
    this.clientService.deleteClient(id).subscribe(dato =>{
      console.log(dato);
      this.findAllClients();
    })
  }

  //Metodo exportar clientes
  export( data: Client[]){
    console.log(data);
    new AngularCsv(data, 'My Report');
  }
}
