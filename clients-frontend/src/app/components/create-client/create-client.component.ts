import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/model/client';
import { ClientService } from 'src/app/services/client.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.css']
})
export class CreateClientComponent implements OnInit {

  client : Client = new Client();
  constructor(private clientService:ClientService,private router:Router) { }

  ngOnInit(): void {
  }

  //metodo guardado clientes
  saveClient(){
    this.clientService.saveClient(this.client).subscribe(dato => {
      console.log(dato);
      this.navigateToClients();
    },error => console.log(error));
  }

  //metodo para volver a lista
  navigateToClients(){
    this.router.navigate(['/clients']);
    swal('Empleado registrado',`El empleado ${this.client.name} ha sido registrado con exito`,`success`);
  }

  onSubmit(){
    this.saveClient();
  }
  //metodo validar solo numeros input
  validateFormat(event) {
    let key;
    if (event.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
    } else {
      key = event.keyCode;
      key = String.fromCharCode(key);
    }
    const regex = /[0-9]|\./;
     if (!regex.test(key)) {
      event.returnValue = false;
       if (event.preventDefault) {
        event.preventDefault();
       }
     }
    }

}
