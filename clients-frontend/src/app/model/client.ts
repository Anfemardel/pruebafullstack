export class Client {
    //modelo cliente
    id : number;
    sharedKey : string;
    name : string;
    email : string;
    phone : number;
    dateAt : Date;
    dateEnd : Date;
}
