import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from '../model/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  //URL backend
  private baseURL = "http://localhost:8080/api/v1/clients";
  constructor(private httpClient: HttpClient) { }

  //Metodo consulta clientes
  getClients(): Observable<Client[]>{
    return this.httpClient.get<Client[]>(`${this.baseURL}`)
    
  }
  //Metodo guardar clientes
  saveClient(client:Client) : Observable<Object>{
    return this.httpClient.post(`${this.baseURL}`, client)
  }
  //Metodo eliminar clientes
  deleteClient(id:number): Observable<Object>{
    return this.httpClient.delete(`${this.baseURL}/${id}`);
  }
}
